<?php

/**
 * @file
 * Contains \Drupal\spotify_new_release\Controller\SpotifyController.
 */

namespace Drupal\spotify_new_release\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\spotify_new_release\Entity\SpotifyConection;

class SpotifyController extends ControllerBase {

    public function index() {
        $newReleases = SpotifyConection::getNewRelease();
        return array(
            '#theme' => 'snr_new_release',
            '#newRelease' => $newReleases,
        );
    }

    public function artist($id) {
        $artist = SpotifyConection::getArtist($id);
        return array(
            '#theme' => 'snr_artist',
            '#artist' => $artist,
        );
    }

}
