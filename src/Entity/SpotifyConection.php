<?php

namespace Drupal\spotify_new_release\Entity;

class SpotifyConection{
    const SETTINGS = 'spotify_new_release.settings';

    /**
     * Autenticación
     * @return mixed|void
     */
    private static function auth()
    {
        try {
            $config = \Drupal::config(static::SETTINGS)->get();
            $config['grant_type'] = 'client_credentials';
            $autorization = \Drupal::httpClient()->request('POST', 'https://accounts.spotify.com/api/token', [
                'form_params' => $config
            ]);
            return $response = ['success'=>true, 'data'=>json_decode($autorization->getBody())];
        } catch (ClientException $e) {
            watchdog_exception('http_module', $e->getMessage());
            return ['success'=>false,'error'=>$e->getMessage()];
        }

    }
    
    public static function getNewRelease(){
        $authData = SpotifyConection::auth();
        if($authData['success']){
            try {
                $request = \Drupal::httpClient()->request('GET', 'https://api.spotify.com/v1/browse/new-releases', [
                    'headers' => [
                        'Authorization' => $authData['data']->token_type . ' ' . $authData['data']->access_token
                    ]
                ]);

                $releases = json_decode($request->getBody());
                return $releases;
            } catch (ClientException $e) {
                watchdog_exception('http_module', $e->getMessage());
                return [];
            }
        }else{
            return [];
        }
    }
    
    public static function getArtist($id){
        $authData = SpotifyConection::auth();
        if($authData['success']){
            try {
                $request = \Drupal::httpClient()->request('GET', 'https://api.spotify.com/v1/artists/' . $id, [
                    'headers' => [
                        'Authorization' => $authData['data']->token_type . ' ' . $authData['data']->access_token
                    ]
                ]);

                $artist = json_decode($request->getBody());
                $request = \Drupal::httpClient()->request('GET', 'https://api.spotify.com/v1/artists/' . $id . '/top-tracks?country=CO', [
                    'headers' => [
                        'Authorization' => $authData['data']->token_type . ' ' . $authData['data']->access_token
                    ]
                ]);

                $artist->tracks = json_decode($request->getBody())->tracks;
                return $artist;
            } catch (ClientException $e) {
                watchdog_exception('http_module', $e->getMessage());
                return [];
            }
        }else{
            return [];
        }
    }
}

